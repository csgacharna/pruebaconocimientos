<?php
include("conexion.php");
class pruebadao extends conexion
{
    //Metodo encargado de consultar todos los registros en la tabla persona.
    public function consultar()
    {
        $sql = 'Select * from persona';
        $sentence = $this->conectar()->prepare($sql);
        $sentence->execute();
        //El result nos devuelve un array indexado.
        $result = $sentence->fetchAll(PDO::FETCH_ASSOC);
        $datos = [];
        $i = 0;
        //Se crea un Arreglo.
        foreach ($result as  $item) {
            $datos[$i] = [
                "id_persona" => $item['id_persona'],
                "nombre" => $item['nombre'],
                "apellido" => $item['apellido'],
                "edad" => $item['edad'],
                "sexo" => $item['sexo'],
                "correo" => $item['correo'],
            ];
            $i++;
        }
        //Header para especificar que se va a devolver un JSON.
        header("Content-type: application/json");
        //Se devuelve un valor JSON.
        echo json_encode($datos);
    }

    //Metoco encargado de editar los valores de la tabla.
    public function editarDatos($id, $nombre, $apellido, $edad, $sexo, $correo)
    {
        try {
            $sql = 'Update persona set nombre = :nombre, apellido = :apellido, edad = :edad, sexo = :sexo,
            correo = :correo where id_persona = :id';
            $sentence = $this->conectar()->prepare($sql);
            //El bindParam es para especificar el valor de la variable.
            $sentence->bindParam(':nombre', $nombre);
            $sentence->bindParam(':apellido', $apellido);
            $sentence->bindParam(':edad', $edad);
            $sentence->bindParam(':sexo', $sexo);
            $sentence->bindParam(':correo', $correo);
            $sentence->bindParam(':id', $id);
            $sentence->execute();
            $resultado["codigo"] = 1;
            $resultado["mensaje"] = "Registro modificado correctamente.";
        } catch (Exception $ex) {
            $resultado["codigo"] = -1;
            $resultado["mensaje"] = "Error al modificar.";
        }
        header("Content-type: application/json");
        echo json_encode($resultado);
    }

    //Metodo encargado de eliminar un dato de la tabla.
    public function eliminarDato($id)
    {
        try {
            $sql = 'delete from persona where id_persona = :id';
            $sentencia = $this->conectar()->prepare($sql);
            $sentencia->bindParam(':id', $id);
            $sentencia->execute();
            $array['codigo'] = 1;
            $array['mensaje'] = 'Se elimino con exito';
        } catch (Exception $ex) {
            $array['codigo'] = -1;
            $array['mensaje'] = 'Error al eliminar';
        }
        header('Content-Type:application/json');
        echo json_encode($array);
    }

    //Metoco encargado de insertar un dato a la tabla.
    public function insertarDato($nombre, $apellido, $edad, $sexo, $correo)
    {
        try {
            $sql = 'Insert into persona (nombre, apellido, edad, sexo, correo) values (:nombre, :apellido,:edad, :sexo, :correo)';
            $sentence = $this->conectar()->prepare($sql);
            $sentence->bindParam(':nombre', $nombre);
            $sentence->bindParam(':apellido', $apellido);
            $sentence->bindParam(':edad', $edad);
            $sentence->bindParam(':sexo', $sexo);
            $sentence->bindParam(':correo', $correo);
            $sentence->execute();
            $resultado["codigo"] = 1;
            $resultado["mensaje"] = "Registro modificado correctamente.";
        } catch (Exception $ex) {
            $resultado["codigo"] = -1;
            $resultado["mensaje"] = "Error al modificar.";
        }
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json");
        echo json_encode($resultado);
    }

    //Metoco encargado de realizar el filtro correspondiente.
    public function buscarFiltro($condicion)
    {
        $sql = "Select * from persona where $condicion;";
        $sentence = $this->conectar()->prepare($sql);
        $sentence->execute();
        $result = $sentence->fetchAll(PDO::FETCH_ASSOC);
        $datos = [];
        $i = 0;
        foreach ($result as  $item) {
            $datos[$i] = [
                "id_persona" => $item['id_persona'],
                "nombre" => $item['nombre'],
                "apellido" => $item['apellido'],
                "edad" => $item['edad'],
                "sexo" => $item['sexo'],
                "correo" => $item['correo'],
            ];
            $i++;
        }
        header("Content-type: application/json");
        echo json_encode($datos);
    }
}
