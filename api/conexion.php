<?php
abstract class conexion
{
  //Conexion a la base de datos POSTGRESQL por PDO.
  public function conectar()
  {
    try {
      $cnn = new PDO('pgsql:host=localhost;dbname=caso_practico', 'postgres', 'postgres');
      return $cnn;
    } catch (PDOException $th) {
      return $th->getMessage();
    }
  }
}
