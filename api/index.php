<?php
//Se declaran los headers que nos ayudan a dar permisos de realizar request de diferentes dominios.
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Access-Control-Allow-Origin");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
//Se declara esta variable para obtener el metodo recibido.
$method = $_SERVER['REQUEST_METHOD'];
//Si el metodo es OPTIONS se termina la ejecucion ya que esta peticion no es necesaria.
if ($method == "OPTIONS") {
  die();
}
//Se realiza include del DAO.
include('pruebadao.php');

//Se crea el objeto del DAO.
$objDAO = new pruebadao();

//Se hacen condicionales para verificar si ese parametro esta llegando en la peticion.
//Se valida el proceso para saber que funcion debe realizar.
if (isset($_GET['proceso'])) {
  $proceso = $_GET['proceso'];
  if ($proceso == "consultar") {
    $objDAO->consultar();
  }
  if ($proceso == "eliminar") {
    $id = $_GET['id'];
    $objDAO->eliminarDato($id);
  }
}

if (isset($_POST['proceso'])) {
  $proceso = $_POST['proceso'];
  if ($proceso == "actualizar") {
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $edad = $_POST['edad'];
    $sexo = $_POST['genero'];
    $correo = $_POST['correo'];
    $id = $_POST['id'];
    $objDAO->editarDatos($id, $nombre, $apellido, $edad, $sexo, $correo);
  }
}

if (isset($_POST['proceso'])) {
  $proceso = $_POST['proceso'];
  if ($proceso == "agregar") {
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $edad = $_POST['edad'];
    $sexo = $_POST['sexo'];
    $correo = $_POST['correo'];
    $objDAO->insertarDato($nombre, $apellido, $edad, $sexo, $correo);
  }
}

//El empty se usa para validar los campos y asi realizar la condicion que se hara en el filtro.
if (isset($_POST['proceso'])) {
  $proceso = $_POST['proceso'];
  if ($proceso == "filtrar") {
    if (!empty($_POST['edadFiltro']) && !empty($_POST['sexoFiltro'])) {
      $edad = $_POST['edadFiltro'];
      $sexo = $_POST['sexoFiltro'];
      $condicion = "edad='$edad' and sexo='$sexo'";
      $objDAO->buscarFiltro($condicion);
    } elseif (!empty($_POST['edadFiltro'])) {
      $edad = $_POST['edadFiltro'];
      $condicion = "edad='$edad'";
      $objDAO->buscarFiltro($condicion);
    } elseif (!empty($_POST['sexoFiltro'])) {
      $sexo = $_POST['sexoFiltro'];
      $condicion = "sexo='$sexo'";
      $objDAO->buscarFiltro($condicion);
    } elseif (empty($_POST['edadFiltro']) && empty($_POST['sexoFiltro'])) {
      $objDAO->consultar();
    }
  }
}
