import React, { Component, Fragment } from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import { CSVLink, CSVDownload } from 'react-csv';
import ReactDOM from 'react-dom';

class App extends Component {

  //Se crea el objeto state para manejar todos los datos
  state = {
    listapersonas: [],
    editar: false,
    agregar: false,
    id: null,
    nombre: "",
    apellido: "",
    edad: "",
    genero: "",
    correo: "",
    sexoFiltro: "",
    edadFiltro: "",
    csvData: [],
    headers: [
      { label: 'ID', key: 'id_persona' },
      { label: 'Nombre', key: 'nombre' },
      { label: 'Apellido', key: 'apellido' },
      { label: 'Edad', key: 'edad' },
      { label: 'Sexo', key: 'sexo' },
      { label: 'Correo', key: 'correo' },
    ],
  }

  //Se crea una funcion para mostrar la tabla en la que se cargaran los usuarios.
  renderTabla = () => {
    //Se realiza una copia de la lista de personas.
    const lista = [...this.state.listapersonas]
    //Se realiza el html de la tabla en la que se vera el resultado de una consulta.
    return (
      <table className="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Edad</th>
            <th>Genero</th>
            <th>Correo</th>
            <th>Acciones</th>
          </tr>
        </thead>
        {/*Se recorre la lista para obtener los datos del arreglo*/}
        <tbody>{lista.map((persona, index) => {
          return (<tr key={persona.id_persona}>
            <td>{persona.id_persona}</td>
            <td>{persona.nombre}</td>
            <td>{persona.apellido}</td>
            <td>{persona.edad}</td>
            <td>{persona.sexo}</td>
            <td>{persona.correo}</td>
            <td className="justify-content-center">
              <button className="btn btn-warning btn-sm">
                <a href="javascript:;" onClick={this.verFormulario} data-index={index}>Editar</a>
              </button>&nbsp;&nbsp;
              <button className="btn btn-danger btn-sm">
                <a href="javascript:;" onClick={this.eliminar} data-index={index}>Eliminar</a>
              </button>
            </td>
          </tr>)
        }
        )}</tbody>
      </table>
    )
  }

  //Se crea funcion ver formulario para al momento de editar, obtener los datos de esa persona.
  verFormulario = (evento) => {
    //Obtengo la lista personas.
    const { listapersonas } = this.state;
    //Se obtiene la posicion dentro del arreglo original.
    const index = evento.target.attributes["data-index"].value;
    //Se realiza una copia de listapersonas para que esta no se modifique a medida que se escribe el dato.
    const persona = { ...listapersonas[index] };
    this.setState({
      agregar: false,
      editar: true,
      id: persona.id_persona,
      nombre: persona.nombre,
      apellido: persona.apellido,
      edad: persona.edad,
      genero: persona.sexo,
      correo: persona.correo,
    })
  }

  //Funcion para setear los valores y mostrar el formulario de agregar
  verFormularioAgregar = () => {
    this.setState({
      agregar: true,
      editar: false,
      id: "",
      nombre: "",
      apellido: "",
      edad: "",
      genero: "",
      correo: "",
    })
  }

  //Funcion para mostrar el formulario de agregar o actualizar.
  //Los input siempre llevan un onChange porque a medida que se escribe, se necesita ir cambiando los valores en el state.
  renderFormulario = () => {
    const { nombre, apellido, edad, genero, correo } = this.state;
    return (
      <div className="formulario col-md-8">
        {this.state.editar == true && <h1>Actualizar Datos</h1>}
        {this.state.agregar == true && <h1>Agregar</h1>}
        <div className="row">
          <div className=" row col-md-6 form-group">
            <label className="col-md-4">Nombres:</label>
            <input type="text" id="nombre" value={nombre} onChange={this.controlarLetras} className="form-control col-md-8"></input>
          </div>
          <div className="row col-md-6 form-group">
            <label className="col-md-4">Apellidos:</label>
            <input type="text" id="apellido" value={apellido} onChange={this.controlarLetras} className="form-control col-md-8"></input>
          </div>
        </div>
        <div className="row">
          <div className=" row col-md-6 form-group">
            <label className="col-md-4">Edad:</label>
            <input type="text" id="edad" value={edad} onChange={this.controlarNumeros} className="form-control col-md-8"></input>
          </div>
          <div className=" row col-md-6 form-group">
            <label className="col-md-4">Genero:</label>
            <select id="genero" onChange={this.controlarCambio} value={genero} className="form-control col-md-8">
              <option value="F">Femenino</option>
              <option value="M">Masculino</option>
            </select>
          </div>
        </div>
        <div className="row">
          <div className=" row col-md-12 form-group justify-content-center">
            <label className="col-md-2">Correo:</label>
            <input type="text" id="correo" value={correo} onChange={this.controlarCambio} className="form-control col-md-4"></input>
          </div>
        </div>
        <div className="row">
          <div className=" row col-md-12 form-group justify-content-center">
            {this.state.agregar == true &&
              <button type="button" className="btn btn-success form-control col-md-2" onClick={this.agregar}>AGREGAR</button>
            }
            {this.state.editar == true &&
              <button type="button" className="btn btn-success form-control col-md-2" onClick={this.actualizar}>ACTUALIZAR</button>
            }
          </div>
        </div>
      </div>
      
    )
  }

  //Funcion que recibe un parametro para cambiar el valor de la variable en el State.
  controlarCambio = (evento) => {
    let cambio = {};
    cambio[evento.target.id] = evento.target.value;
    this.setState(cambio);
  }

  //Funcion para validar que en algunos inputs solo se pueda ingresar letras y cambie su valor.
  controlarLetras = (evento) => {
    const validacion = /^[a-zA-Z ]*$/;
    if (evento.target.value === '' || validacion.test(evento.target.value)) {
      let cambio = {};
      cambio[evento.target.id] = evento.target.value;
      this.setState(cambio);
    }
  }

  //Funcion para validar que en algunos inputs solo se pueda ingresar numeros y cambie su valor.
  controlarNumeros = (evento) => {
    const validacion = /^[0-9\b]+$/;
    if (evento.target.value === '' || validacion.test(evento.target.value)) {
      let cambio = {};
      cambio[evento.target.id] = evento.target.value;
      this.setState(cambio);
    }
  }

  //Funcion para obtener parametros y enviarlos al DAO.
  agregar = () => {
    //Se declara una variable con llaves para obtener los valores del objeto state.
    const { nombre, apellido, edad, genero, correo } = this.state;
    //Se crea un objeto de parametros para en el, agregar todos los datos.
    var params = new URLSearchParams();
    params.append('proceso', 'agregar');
    params.append('nombre', nombre);
    params.append('apellido', apellido);
    params.append('edad', edad);
    params.append('correo', correo);
    params.append('sexo', genero);
    //Mediante una promesa accedemos a la ruta y enviamos los respectivos parametros.
    axios.post("http://localhost:8080/nuevo/api/", params)
      .then(respuesta => {
        //Si la repuesta fue correcta se vuelve a ocultar el formulario y se muestra la consulta.
        if (respuesta.data.codigo == 1) {
          this.setState({
            agregar: false
          });
          this.consultar();
        }
      });
  }

  //Funcion para obtener parametros y enviarlos al DAO.
  actualizar = () => {
    //Se declara una variable con llaves para obtener los valores del objeto state.
    const {id, nombre, apellido, edad, genero, correo } = this.state;
    //Se crea un objeto de parametros para en el, agregar todos los datos.
    var params = new URLSearchParams();
    params.append('proceso', 'actualizar');
    params.append('nombre', nombre);
    params.append('apellido', apellido);
    params.append('edad', edad);
    params.append('correo', correo);
    params.append('genero', genero);
    params.append('id', id);
    //Mediante una promesa accedemos a la ruta y enviamos los respectivos parametros.
    axios.post("http://localhost:8080/nuevo/api/", params)
      .then(respuesta => {
        //Si la repuesta fue correcta se vuelve a ocultar el formulario y se muestra la consulta.
        if (respuesta.data.codigo == 1) {
          this.setState({
            editar: false
          });
          this.consultar();
        }
      });
  }

  //Funcion para eliminar un registro.
  eliminar = (evento) => {
    const { listapersonas } = this.state;
    //Se obtiene un atributo del boton para asi encontrar el id.
    const index = evento.target.attributes["data-index"].value;
    const id = listapersonas[index].id_persona;
    //Mediante una promesa accedemos a la ruta y enviamos los respectivos parametros.
    axios.get("http://localhost:8080/nuevo/api/?proceso=eliminar&id=" + id)
      .then(res => {
        if (res.data.codigo == 1) {
          this.consultar();
        }
      });

  }

  //Funcion para el momento en que se filtren los resultados
  filtrar = () => {
    //Se declara una variable con llaves para obtener los valores del objeto state.
    const { edadFiltro, sexoFiltro } = this.state;
    //Se crea un objeto de parametros para en el agregar todos los datos.
    var params = new URLSearchParams();
    params.append('proceso', 'filtrar');
    params.append('edadFiltro', edadFiltro);
    params.append('sexoFiltro', sexoFiltro);
    //Mediante una promesa accedemos a la ruta y enviamos los respectivos parametros.
    axios.post("http://localhost:8080/nuevo/api/", params)
      .then(respuesta => {
        //Se setean en el state la variable listapersona la cual arma la tabla y la variable csvData que construye el csv.
        this.setState({
          listapersonas: respuesta.data,
          csvData: respuesta.data,
        });
      })
  }

  //Funcion para realizar la consulta general a la BD.
  consultar = () => {
    //Mediante una promesa accedemos a la ruta.
    axios.get("http://localhost:8080/nuevo/api/?proceso=consultar")
      .then(respuesta => {
        //Se setean en el state la variable listapersona la cual arma la tabla y la variable csvData que construye el csv.
        this.setState({
          listapersonas: respuesta.data,
          csvData: respuesta.data,
        });
      })
  }

  //El render devuelve un jsx.
  render() {
    //Se retorna toda la vista.
    return (
      <Fragment>
        <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
          <a className="navbar-brand" href="#">Caso Practico</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <a className="nav-link" href="#">Inicio <span className="sr-only">(current)</span></a>
              </li>
              <li onClick={this.verFormularioAgregar} id="agregar">
                <a className="nav-link">Agregar</a>
              </li>
            </ul>
          </div>
        </nav>

        <div id="tablaDatos">
          <div className="row col-md-12 justify-content-center" id="filtros">
            <label className="col-md-1">Edad:</label>
            <input type="text" id="edadFiltro" value={this.state.edadFiltro} onChange={this.controlarNumeros} className="form-control col-md-3"></input>
            <label className="col-md-1">Sexo:</label>
            <select className="col-md-3 form-control" id="sexoFiltro" value={this.state.sexoFiltro} onChange={this.controlarCambio}>
              <option value="" >Seleccionar</option>
              <option value="F">Femenino</option>
              <option value="M">Masculino</option>
            </select>&nbsp;&nbsp;
            <button className="btn btn-info col-md-2" onClick={this.filtrar}>Buscar</button>
          </div>

          {this.renderTabla()}
          <div className="row justify-content-end" id="exportar">
            {/*Se crea componente CSVLink para la exportacion del archivo csv con sus props*/}
            <CSVLink data={this.state.listapersonas}
              headers={this.state.headers}
              filename={"usuarios.csv"}
              className="btn btn-primary"
              target="_blank" >Exportar CSV</CSVLink>
          </div>
        </div>
        {(this.state.agregar == true || this.state.editar == true) && this.renderFormulario()}
      </Fragment>
    )
  };

  //Metodo que se ejecuta al momento de que se carga el componente.
  componentDidMount() {
    this.consultar()
  }
}

export default App;
